//
//  IncomeTableViewCell.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit

class IncomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!

    func configureCell(item: Income) {
        
        nameLabel.text = item.name
        amountLabel.text = "$" + String(item.amount)
        dateLabel.text = String(describing: item.incomeDate)
        frequencyLabel.text = item.frequency
        
    }

    
}
