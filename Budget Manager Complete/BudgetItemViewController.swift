//
//  BudgetItemViewController.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit
import CoreData

class BudgetItemViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    
    var itemToEdit: BudgetItems?
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
    
        var item: BudgetItems!
        
        if itemToEdit == nil {
            
            item = BudgetItems(context: context)
            
        } else {

        
        item = itemToEdit
       
        }
        
        if let name = nameField.text {
            
            item.name = name
        }
        
        if let amount = amountField.text {
            
            item.amount = (amount as NSString).doubleValue
            
        }
        
        ad.saveContext()
        
    
        
        performSegue(withIdentifier: "saveButtonTapped", sender: UIButton.self)
    }
    
    func loadItemData() {
        
        if let item = itemToEdit {
            
            nameField.text = item.name
            amountField.text = "\(item.amount)"
        }
        
        
}

    
    @IBAction func deleteButtonPressed(_ sender: UIBarButtonItem) {
        
        if itemToEdit != nil {
            
            context.delete(itemToEdit!)
            ad.saveContext()
            print("hello")
        }
        
        performSegue(withIdentifier: "delete", sender: UIBarButtonItem.self)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if itemToEdit != nil {
            loadItemData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
