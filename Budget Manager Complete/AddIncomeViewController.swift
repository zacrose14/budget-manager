//
//  AddIncomeViewController.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit
import CoreData

class AddIncomeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var frequencyPicker: UIPickerView!
    
    var incomeItemToEdit: Income?
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        var item: Income!
        
        if incomeItemToEdit == nil {
            
            item = Income(context: context)
            
        } else {
            
            
            item = incomeItemToEdit
            
        }
        
        if let name = nameField.text {
            
            item.name = name
        }
        
        if let amount = amountField.text {
            
            item.amount = (amount as NSString).doubleValue
            
        }
        
        item.frequency = String(frequencyPicker.selectedRow(inComponent: 0))
        
        item.incomeDate = datePicker.date as NSDate
        
        ad.saveContext()
        
        performSegue(withIdentifier: "incomeSaved", sender: UIButton.self)
        
    }
    
    let frequencyOptions = ["Weekly","Biweekly","Monthly","Quarterly","Semiannual","Annual","None"]
    
    @IBAction func deleteButtonPressed(_ sender: UIBarButtonItem) {
        
        if incomeItemToEdit != nil {
            
            context.delete(incomeItemToEdit!)
            ad.saveContext()
            print("hello")
        }

        performSegue(withIdentifier: "incomeDelete", sender: UIBarButtonItem.self)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        frequencyPicker.dataSource = self
        frequencyPicker.delegate = self
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequencyOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return frequencyOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // update when seelcted
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
 
}
