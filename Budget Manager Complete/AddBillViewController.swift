//
//  AddBillViewController.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit
import CoreData

class AddBillViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

   
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var dueDatePicker: UIDatePicker!
    @IBOutlet weak var frequencyPicker: UIPickerView!
    
    var billItemToEdit: Bills?
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        var item: Bills!
        
        if billItemToEdit == nil {
            
            item = Bills(context: context)
            
        } else {
            
            
            item = billItemToEdit
            
        }
        
        if let name = nameTextField.text {
            
            item.name = name
        }
        
        if let amount = amountField.text {
            
            item.amount = (amount as NSString).doubleValue
            
        }
        
        item.frequency = String(frequencyPicker.selectedRow(inComponent: 0))
        
        item.dueDate = dueDatePicker.date as NSDate
        
        ad.saveContext()
        
        performSegue(withIdentifier: "billUpdated", sender: UIButton.self)

    }
    
    
    func loadItemData() {
        
        if let item = billItemToEdit {
            
            nameTextField.text = item.name
            amountField.text = "\(item.amount)"
        }
        
    }
    
    let frequencyOptions = ["Weekly","Biweekly","Monthly","Quarterly","Semiannual","Annual","None"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frequencyPicker.dataSource = self
        frequencyPicker.delegate = self
        
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequencyOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return frequencyOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // update when seelcted
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIBarButtonItem) {
        
        if billItemToEdit != nil {
            
            context.delete(billItemToEdit!)
            ad.saveContext()
            print("hello")
        }
        
        performSegue(withIdentifier: "billDelete", sender: UIBarButtonItem.self)
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
}
