//
//  IncomeViewController.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit
import CoreData

class IncomeViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var incomeView: UITableView!
    
    
    var controller: NSFetchedResultsController<Income>!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            incomeView.dataSource = self
            incomeView.delegate = self
            
            attemptFetch()
            //generateTestData()
            
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if let sections = controller.sections {
                
                let sectionInfo = sections[section]
                return sectionInfo.numberOfObjects
                
            }
            
            return 0
        }
        
        
        func numberOfSections(in tableView: UITableView) -> Int {
            if let sections = controller.sections {
                return sections.count
                
            }
            
            return 0
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = incomeView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath) as! IncomeTableViewCell
            configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            return cell
        }
        
        func configureCell(cell: IncomeTableViewCell, indexPath: NSIndexPath) {
            
            let item = controller.object(at: indexPath as IndexPath)
            cell.configureCell(item: item)
            
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            
            let item = objs[indexPath.row]
            performSegue(withIdentifier: "addIncome", sender: item)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addIncome" {
            if let destination = segue.destination as? AddIncomeViewController {
                if let item = sender as? Income {
                    destination.incomeItemToEdit = item
                }
            }
        }
        
    }

    
        func attemptFetch() {
            
            let fetchRequest: NSFetchRequest<Income> = Income.fetchRequest()
            let nameSort = NSSortDescriptor(key: "name", ascending: false)
            fetchRequest.sortDescriptors = [nameSort]
            
            let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            
            controller.delegate = self
            
            self.controller = controller
            
            do {
                
                try controller.performFetch()
                
            } catch {
                
                let error = error as NSError
                print("\(error)")
                
            }
        }
        func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
            
            incomeView.beginUpdates()
            
        }
        
        func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
            
            incomeView.endUpdates()
        }
        
        private func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeanObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
            
            switch(type) {
                
            case.insert:
                if let indexPath = newIndexPath {
                    
                    incomeView.insertRows(at: [indexPath], with: .fade)
                }
                break
            case.delete:
                if let indexPath = indexPath {
                    incomeView.deleteRows(at: [indexPath], with: .fade)
                    
                }
                break
            case.update:
                if let indexPath = indexPath {
                    
                    let cell = incomeView.cellForRow(at: indexPath) as! IncomeTableViewCell
                    configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
                }
                break
            case.move:
                if let indexPath = indexPath {
                    
                    incomeView.deleteRows(at: [indexPath], with: .fade)
                }
                if let indexPath = newIndexPath {
                    incomeView.insertRows(at: [indexPath], with: .fade)
                }
                break
            }
        }
        
        func generateTestData() {
            
            let item = Income(context: context)
            item.name = "Adrienne's Paycheck"
            item.amount = 3000
            
            item.frequency = "biweekly"
            
        }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
}
