//
//  BillsTableViewCell.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit

class BillsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    
    func configureCell(item: Bills) {
        
        nameLabel.text = item.name
        amountLabel.text = String(item.amount)
        dueLabel.text = String(describing: item.dueDate)
        frequencyLabel.text = item.frequency
        
    }

}
