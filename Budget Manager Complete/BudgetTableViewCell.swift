//
//  BudgetTableViewCell.swift
//  Budget Manager Complete
//
//  Created by Zac Rose on 4/5/17.
//  Copyright © 2017 Zac Rose. All rights reserved.
//

import UIKit

class BudgetTableViewCell: UITableViewCell {

    
    @IBOutlet weak var budgetName: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    
    func configureCell(item: BudgetItems) {
        
        budgetName.text = item.name
        amountLabel.text = String(item.amount)
        
        
    }
    

}
